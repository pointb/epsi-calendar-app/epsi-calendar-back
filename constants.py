from model.Day import Day
from datetime import date

DAYS = [
    Day(name="Monday", min_screen_position=100, max_screen_position=105),
    Day(name="Tuesday", min_screen_position=115, max_screen_position=125),
    Day(name="Wednesday", min_screen_position=130, max_screen_position=145),
    Day(name="Thursday", min_screen_position=150, max_screen_position=165),
    Day(name="Friday", min_screen_position=165, max_screen_position=182),
    Day(name="Saturday", min_screen_position=182, max_screen_position=190)
]

YEAR_START_DATE = date(2020, 9, 29)
YEAR_END_DATE = date(2021, 4, 5)

SCHEDULE_URL = "https://edtmobiliteng.wigorservices.net//WebPsDyn.aspx"

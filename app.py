import re
from datetime import timedelta

import requests
from bs4 import BeautifulSoup
from flask import Flask
from flask_socketio import SocketIO, emit

from constants import DAYS, YEAR_START_DATE, YEAR_END_DATE, SCHEDULE_URL
from model.Course import Course

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socket_io = SocketIO(app, cors_allowed_origins="*")


@socket_io.on('download')
def handle_download_event(user):
    download_courses(user)


def get_date_first_weekday(date):
    return date - timedelta(days=date.weekday() % 7)


def get_schedule_week_data(user, date):
    print(user)
    url = f"{SCHEDULE_URL}?action=posEDTBEECOME&serverid=C&Tel=valentin.harny&date={date.strftime('%m/%d/%Y')}"
    print(url)
    r = requests.get(url)
    return r.content


def is_empty_week(case):
    return True if case.get_text() == "Pas de cours cette semaine" else False


def get_duration(time):
    def convert_hour_to_seconds(hour):
        (h, m) = hour.split(':')
        return int(h) * 3600 + int(m) * 60

    start = convert_hour_to_seconds(time.split("-")[0][:-1])
    end = convert_hour_to_seconds(time.split("-")[1][1:])

    return round((end - start) / 3600, 2)


def format_data(courses):
    data = {}
    for course in courses:
        course = course.to_dict()
        print(course)
        data.setdefault(course['date'], []).append(course)
    return data


def get_data(data, case):
    try:
        if data == "screen_position":
            return float(re.search(r'left:(.*?)%', str(case)).group(1))
        elif data == "name":
            return case.find("div", {"class": "Teams"}).next_sibling
        elif data == "teacher":
            return case.find("td", {"class": "TCProf"}).next_element.strip().title()
        elif data == "group":
            return case.find("td", {"class": "TCProf"}).contents[2].strip()
        elif data == "classroom":
            return case.find("td", {"class": "TCSalle"}).get_text()
        elif data == "time":
            return case.find("td", {"class": "TChdeb"}).get_text()
        elif data == "color":
            return re.search(r'background-color:(.*?);', str(case)).group(1)
        else:
            return None
    except Exception as e:
        print(e)
        return None


def download_courses(user):
    print(user)
    start_date = get_date_first_weekday(YEAR_START_DATE)
    courses = []

    while start_date < YEAR_END_DATE:
        emit("progress", start_date.strftime("%d/%m/%Y"))

        soup = BeautifulSoup(get_schedule_week_data(user, start_date), "html.parser")
        cases = soup.find_all("div", {"class": "Case"})

        if cases:
            for case in cases:
                screen_position = get_data("screen_position", case)
                if screen_position:
                    for i, day in enumerate(DAYS):
                        if day.min_screen_position < screen_position < day.max_screen_position:
                            date = (start_date + timedelta(days=i)).strftime("%Y-%m-%d")
                            name = get_data("name", case)
                            teacher = get_data("teacher", case)
                            group = get_data("group", case)
                            classroom = get_data("classroom", case)
                            time = get_data("time", case)
                            print(time)
                            duration = get_duration(time)
                            color = get_data("color", case)

                            course = Course(date, name, teacher, group, classroom, time, duration, color)
                            courses.append(course)
                            break

        start_date += timedelta(weeks=1)

    emit('complete', format_data(courses))


if __name__ == '__main__':
    socket_io.run()

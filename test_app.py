from unittest import TestCase
from datetime import date
import app


class Test(TestCase):
    def test_get_date_first_weekday(self):
        actual_date = date(2021, 3, 12)
        expected_first_weekday = date(2021, 3, 8)
        dt = app.get_date_first_weekday(actual_date)
        self.assertEqual(dt, expected_first_weekday)

    def test_get_duration(self):
        time = "09:30 - 12:15"
        expected_duration = 2.75
        duration = app.get_duration(time)
        self.assertEqual(duration, expected_duration)

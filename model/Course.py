class Course:
    def __init__(self, date, name, teacher, group, classroom, time, duration, color):
        self.date = date
        self.name = name
        self.teacher = teacher
        self.group = group
        self.classroom = classroom
        self.time = time
        self.duration = duration
        self.color = color

    def to_dict(self):
        return vars(self)
